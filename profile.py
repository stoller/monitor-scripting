"""
This profile will periodically run the RF monitor on a set of radios
using the XMLRPC interface to the Powder control framework. The set of
radios is specified in a json file, there is an example in the repo. A
new experiment is started for each radio, the monitor is run once, the
data file is copied back, and the experiment is terminated. 

This profile is instantiated on a single generic compute node, it
does not need to be anything fancy, since it all it does is invoke
the XMLRPC interface and copy back the data files. 

Instructions:
Once your node boots, log in and make a copy of the json file:
```
        cp /local/repository/radios.json /tmp/radios.json
```
Edit this file, you will of course want to choose radios/nodes you are
allowed to use. If you choose a node that is not in service, it will be
skipped and an error generated. Then start the driver script:
```
        /local/repository/runmonitor.pl -p PID /tmp/radios.json
```
where the PID is a project name you are a member of and has permission to use
the radios. The script runs in the foreground and sends status info to
STDOUT/STDERR.

The data files are collected from the target radios (compute nodes)
and stored in `/local/www/` on your node. You can browse the
[result frequency graphs](http://{host-node}:7998/frequency-graphs.html).
You will need to refresh the page occasionally to see newly added graphs. 
"""

# Import the Portal object.
import geni.portal as portal
# Import the ProtoGENI library.
import geni.rspec.pg as pg
# Emulab specific extensions.
import geni.rspec.emulab as emulab

# Create a portal context, needed to defined parameters
pc = portal.Context()

# Create a Request object to start building the RSpec.
request = pc.makeRequestRSpec()

# Need just one node to run the tools
node = request.RawPC("node");

#
# Install and start X11 VNC. Calling this informs the Portal that you want
# a VNC option in the node context menu to create a browser VNC client.
#
node.startVNC()

#
# Install support code.
#
node.addService(
    pg.Execute(shell="sh", command="/local/repository/install.sh"))

# Print the RSpec to the enclosing page.
pc.printRequestRSpec(request)
